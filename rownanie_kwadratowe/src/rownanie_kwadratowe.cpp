#include <iostream>
#include <cmath>
using namespace std;

int main() {

	cout << "Program rozwiazujacy rownanie kwadratowe w postaci ax^2+bx+c=0" //uwzglednia warunki zalezne od wielkosci delty
			<< endl;
	int a;
	int b;
	int c;

	cout << "Podaj wartosc a: ";
	cin >> a;
	cout << "Podaj wartosc b: ";
	cin >> b;
	cout << "Podaj wartosc c: ";
	cin >> c;

	double delta;
	double x1;
	double x2;
	double x0;

	delta = pow(b, 2) - 4 * a * c;

	cout << "Delta = " << delta << endl;
	if (delta > 0) {
		x1 = ((-b - sqrt(delta)) / (2 * a));
		x2 = ((-b + sqrt(delta)) / (2 * a));
		cout << "x1 = " << x1 << endl;
		cout << "x2 = " << x2 << endl;
	} else if (delta == 0) {
		x0 = -b / (2 * a);
		cout << "x0 = " << x0 << endl;
	} else {
		cout << "Delta ujemna, brak mniejsc zerowych" << endl;
	}

return 0;
}
