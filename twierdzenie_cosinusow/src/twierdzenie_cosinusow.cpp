#include <iostream>
#include <cmath>
using namespace std;

int main() {
cout << "Twierdzenie cosinusow:" << endl;
	int k;
	int l;
	float m;
	int gamma;

	cout << "Podaj dlugosc boku a : ";
	cin >> k;
	cout << "Podaj dlugosc boku b: ";
	cin >> l;
	cout << "Podaj kat miedzy nimi w stopniach: ";
	cin >> gamma;
	m = sqrt(pow(k, 2) + pow(l, 2) - 2 * k * l * cos(gamma * M_PI / 180));
	cout << "Dlugosc boku c wynosi: " << m << endl;

	return 0;
}
