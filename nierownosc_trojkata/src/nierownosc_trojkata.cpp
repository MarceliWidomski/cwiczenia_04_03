
#include <iostream>
#include <cmath>
using namespace std;

int main() {

	cout << "Program sprawdzajacy nierownosc trojkata" << endl;
	int a;
	int b;
	int c;
	int temp; // zmienna tymczasowa dla podmiany wartosci w intrukcjach warunkowych

	cout << "Podaj dlugosc boku a: ";
	cin >> a;
	cout << "Podaj dlugosc boku  b: ";
	cin >> b;
	cout << "Podaj dlugosc boku c: ";
	cin >> c;

	if (a > b) {
		temp = a;
		a = b;
		b = temp;
	}

	if (b > c) {
		temp = b;
		b = c;
		c = temp;
	}
	if (a > b) {
		temp = a;
		a = b;
		b = temp;
	}

	if (a + b >= c) {
		cout << "Nierownosc trojkata jest spelniona." << endl;
	} else {
		cout << "Nierownosc trojkata jest niespelnona." << endl;
	}
	return 0;
}
