#include <iostream>
#include <cmath>
using namespace std;

int main() {
	int a;
	int b;
	float c;
	cout << "Twierdzenie Pitagorasa:" << endl;
	cout << "Podaj dlugosc pierwszej przyprostokatnej: ";
	cin >> a;
	cout << "Podaj dlugosc drugiej przyprostokatnej: ";
	cin >> b;
	c = sqrt(pow(a, 2) + pow(b, 2));
	cout << "Dlugosc przeciwprostokatnej jest rowna: " << c << endl;

	int x;
	int y;
	float z;

	cout << "Twierdzenie odwrotne do twierdzenia Pitagorasa:" << endl;
	cout << "Podaj dlugosc przeciwprostokatnej : ";
	cin >> x;
	cout << "Podaj dlugosc drugiej przyprostokatnej: ";
	cin >> y;
	z = sqrt(pow(x, 2) - pow(y, 2));
	cout << "Dlugosc przeciwprostokatnej jest rowna: " << z << endl;
	return 0;
}
